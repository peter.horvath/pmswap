#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  unsigned int s;
  void* cp;
  cp = calloc(1000000, 1);
  printf ("a big calloc: %p\n", cp);
  free(cp);
  for (uint64_t n = 0; n <= 29; n++) {
    s = 1ULL << n;
    void* addr = malloc(s);
    printf ("%*u bytes, address: %p\n", 9, s, addr);
  };
  return 0;
};
