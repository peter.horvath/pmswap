#!/bin/bash
usage() {
  echo "$0: Hooks malloc/mmap-related glibc calls to file-backed mmaps.

  Useful in memory-limited paravirtualization environments.
  
  Arguments:

  --min-size 4096        (file-backed allocations only from this size)
  --tmp-dir /tmp         (temporary file location)
  --log                  (enable debug logging to stderr)
  --dry                  (does nothing, only shows what will be done)
  --hooklib libpmswap.so (use alternate debug library)
  --help                 (shows this help)
  --                     (no more arguments for pmswap)
  program
  args...
" >&2
  exit 0
}

die() {
  echo "$0: FATAL: $*" >&2
  exit 1
}

MIN_SIZE=""
TMP_DIR=""
LOG=n
DRY=n
HOOKLIB=""

while [ "$1" != "" ]
do
  if [ "$1" = "--min-size" ]
  then
    shift
    MIN_SIZE="$1"
    shift
    continue
  elif [ "$1" = "--tmp-dir" ]
  then
    shift
    TMP_DIR="$1"
    shift
    continue
  elif [ "$1" = "--log" ]
  then
    LOG=y
    shift
    continue
  elif [ "$1" = "--dry" ]
  then
    DRY=y
    shift
    continue
  elif [ "$1" = "--hooklib" ]
  then
    shift
    HOOKLIB="$1"
    shift
    continue
  elif [ "$1" = "--" ]
  then
    shift
    break
  elif [ "${1:0:1}" =  "-" ]
  then
    usage
  else
    break
  fi
done

[ "$1" = "" ] && usage

COMMAND=""

if [ "$MIN_SIZE" != "" ]
then
  COMMAND="${COMMAND} PMS_MIN_SIZE=${MIN_SIZE}"
fi

if [ "$TMP_DIR" != "" ]
then
  COMMAND="${COMMAND} PMS_TMP_DIR=${TMP_DIR}"
fi

if [ "$PMG_LOG" = "y" ]
then
  COMMAND="${COMMAND} PMG_LOG=1"
fi

if [ "$HOOKLIB" != "" ]
then
  COMMAND="${COMMAND} LD_PRELOAD=${HOOKLIB}"
else
  COMMAND="${COMMAND} LD_PRELOAD=libpmswap.so"
fi

COMMAND="${COMMAND:1}"

if [ "$DRY" = "y" ]
then
  echo "${COMMAND} exec $@"
else
  eval "${COMMAND}" exec "$@"
fi
