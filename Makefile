#!/usr/bin/make -f
CC:=gcc

.PHONY: default
default: lib/libpmswap.so lib/libpmswap_dbg.so

lib/libpmswap_dbg.so: lib/pmswap.c
	$(CC) -shared -o $@ $< -fPIC -Wall -ldl -g -O0

lib/libpmswap.so: lib/pmswap.c
	$(CC) -shared -o $@ $< -fPIC -Wall -ldl -s -Os

.PHONY: test
test: test/test

.PHONY: test/test
test/test: test/test.c lib/libpmswap_dbg.so
	gcc -o $@ $< -Wall
	PMS_LOG=1 LD_PRELOAD=./lib/libpmswap_dbg.so ./test/test

.PHONY: clean
clean:
	rm -vf lib/libpmswap_dbg.so lib/libpmswap.so test/test
