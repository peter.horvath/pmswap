#define _GNU_SOURCE

#include <dirent.h>
#include <dlfcn.h>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <stdarg.h>
#include <stdatomic.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/cdefs.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>

/* That means that long long is not 8-byte, so we can not compile this */
#define STATIC_ASSERT(condition) typedef char __CONCAT(_static_assert_, __LINE__)[ (condition) ? 1 : -1]
STATIC_ASSERT(sizeof(unsigned long long int) == 8);

#if ! __GLIBC_PREREQ(2,30)

#include <sys/syscall.h>

static pid_t gettid(void) {
  return syscall(SYS_gettid);
}

static int renameat2(int olddirfd, const char *oldpath, int newdirfd, const char *newpath, unsigned int flags) {
  return syscall(SYS_renameat2, olddirfd, oldpath, newdirfd, newpath, flags);
}

#endif

#ifndef RENAME_NOREPLACE
#define RENAME_NOREPLACE (1 << 0)
#endif

#ifndef MAP_SHARED_VALIDATE
#define MAP_SHARED_VALIDATE 0x03
#endif


#if ! ATOMIC_LLONG_LOCK_FREE
#error Need lock-free LLONG
#endif

#define BUF_SIZE 2048
#define DEFAULT_TMP_DIR "/tmp"
#define DEFAULT_MIN_SIZE 4096

/* Comes from that ~0ULL printf-ed as "%p" is 22 bytes long */
#define MAX_NAME_LENGTH 23

atomic_ullong counter = 0;

static char* prefix = NULL;
static unsigned int page_size = 0;

typedef void* (*malloc_fn_t)(size_t size);
typedef void (*free_fn_t)(void* ptr);
typedef void* (*realloc_fn_t)(void* ptr, size_t size);
typedef void* (*calloc_fn_t)(size_t nmemb, size_t size);
typedef void* (*mmap_fn_t)(void* addr, size_t length, int prot, int flags, int fd, off_t offset);
typedef int (*munmap_fn_t)(void* addr, size_t length);
typedef void* (*mremap_fn_t)(void *old_address, size_t old_size, size_t new_size, int flags, ...);

static malloc_fn_t orig_malloc = NULL;
static free_fn_t orig_free = NULL;
static realloc_fn_t orig_realloc = NULL;
static calloc_fn_t orig_calloc = NULL;
static mmap_fn_t orig_mmap = NULL;
static munmap_fn_t orig_munmap = NULL;
static mremap_fn_t orig_mremap = NULL;

static int pms_inited = 0;
static int pms_logging = 0;
static int pms_dirfd = -1;
static size_t pms_min_size = DEFAULT_MIN_SIZE;

#define DEBUG(...) { if (pms_logging) { fprintf (stderr, "%i: ", gettid()); fprintf(stderr, __VA_ARGS__);} } 

static __attribute__((noreturn)) void die(char* msg) {
  fprintf (stderr, "%s (errno=%i, %s)\n", msg, errno, strerror(errno));
  exit(errno);
}

static size_t pms_adjust_size(size_t size) {
  if (size % page_size) {
    size_t oldSize = size;
    size = (size | (page_size - 1)) + 1;
    DEBUG("size %zu adjuested to %zu\n", oldSize, size);
  }
  return size;
}

static void pms_rename(char* srcName, char* dstName) {
  if (renameat2(pms_dirfd, srcName, pms_dirfd, dstName, RENAME_NOREPLACE) < 0)
    die ("renameat2 fails");
}

/* Private glibc function not using calloc() for name resolution */
extern void* _dl_sym(void *handle, const char *name, void *who);

static void pms_import(void** fn_ptr, char* name) {
  if (!*fn_ptr)
    *fn_ptr = _dl_sym(RTLD_NEXT, name, "dlsym");
  if (!*fn_ptr)
    die ("can not find symbol");
}

static __attribute__((constructor)) void pms_init() {
  if (pms_inited)
    return;

  page_size = sysconf(_SC_PAGESIZE);
  if (page_size == -1)
    die("can not get _SC_PAGESIZE, problem");
  if (getenv("PMS_LOG"))
    pms_logging = 1;

  pms_import((void*)&orig_malloc, "malloc");
  pms_import((void*)&orig_free, "free");
  pms_import((void*)&orig_realloc, "realloc");
  pms_import((void*)&orig_calloc, "calloc");
  pms_import((void*)&orig_mmap, "mmap");
  pms_import((void*)&orig_munmap, "munmap");
  pms_import((void*)&orig_mremap, "mremap");

  DEBUG ("pid=%i\n", getpid());

  char* min_size_str = getenv("PMS_MIN_SIZE");
  if (min_size_str)
    pms_min_size = atol(min_size_str);

  char prefix_buf[BUF_SIZE];
  char* pms_tmp_dir = getenv("PMS_TMP_DIR");
  if (!pms_tmp_dir)
    pms_tmp_dir = DEFAULT_TMP_DIR;
  for (int l = strlen(pms_tmp_dir) - 1; l >= 0 && pms_tmp_dir[l] == '/'; l--) {
    pms_tmp_dir[l] = 0;
  }
  snprintf(prefix_buf, BUF_SIZE - 1, "%s/%i", pms_tmp_dir, getpid());

  prefix = orig_malloc(strlen(prefix_buf) + 1);
  if (!prefix)
    die ("orig_malloc serious problem");
  strcpy(prefix, prefix_buf);

  if (mkdir (prefix, S_IRWXU)) {
    if (errno == EEXIST)
      errno = 0;
    else
      die ("can not mkdir tmp directory");
  }
  pms_dirfd = open(prefix, O_CLOEXEC | O_DIRECTORY);
  if (pms_dirfd < 0)
    die ("can not open tmp dir");

  pms_inited = 1;
}

static int pms_find(void* ptr, struct stat *status, char* name) {
  int savederrno = errno;
  snprintf (name, MAX_NAME_LENGTH - 1, "%p", ptr);
  int ret = fstatat(pms_dirfd, name, status, 0);
  if (ret == -1) {
    if (errno == ENOENT) {
      errno = savederrno;
      return 0;
    } else
      die ("stat tmp file gives some error");
  } else
    return 1;
}

static void* pms_mmap(void* addr, size_t size, int prot, int flags) {
  char tmpName[MAX_NAME_LENGTH];
  char endName[MAX_NAME_LENGTH];

  unsigned long long int id = atomic_fetch_add_explicit(&counter, 1, memory_order_relaxed);
  snprintf(tmpName, MAX_NAME_LENGTH - 1, "%llu", id);

  size = pms_adjust_size(size);

  int fd = openat(pms_dirfd, tmpName, O_CREAT|O_EXCL|O_NOATIME|O_LARGEFILE|O_NOCTTY|O_RDWR, S_IRWXU);
  if (fd < 0)
    die ("can not create tmp file");
  
  if (ftruncate(fd, size))
    die ("can not increase file size");

  void* ret_addr = orig_mmap(addr, size, prot, flags, fd, 0);
  if (ret_addr == MAP_FAILED)
    die ("mmap tmp file fails");

  snprintf(endName, MAX_NAME_LENGTH - 1, "%p", ret_addr);

  pms_rename(tmpName, endName);

  if (close(fd))
    die ("close fails");

  return ret_addr;
};

static void* pms_malloc(size_t size) {
  return pms_mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_NONBLOCK|MAP_NORESERVE);
};

/* Order is important! (Imagine if multiple threads malloc/free quickly the same area...) */
static void pms_free(void* ptr, struct stat *status, char* name) {
  if (unlinkat(pms_dirfd, name, 0) == -1)
    die ("pms_free unlink fails");
  if (orig_munmap(ptr, status->st_size))
    die ("munmap fails");
};

void* malloc(size_t size) {
  DEBUG("called malloc %zu\n", size);
  pms_init();

  if (size < pms_min_size)
    return orig_malloc(size);
  else
    return pms_malloc(size);
};

void free(void* ptr) {
  DEBUG("called free %p\n", ptr);
  pms_init();

  if (!ptr)
    return;

  char name[MAX_NAME_LENGTH];
  struct stat status;
  if (pms_find(ptr, &status, name))
    pms_free(ptr, &status, name);
  else
    orig_free(ptr);
};

void cfree(void* ptr) {
  die("calling cfree is not allowed\n");
};

void* calloc(size_t nmemb, size_t size) {
  DEBUG("called calloc %zu * %zu\n", nmemb, size);
  pms_init();

  uint64_t _size = (uint64_t)nmemb * size;
  if (_size < pms_min_size && orig_calloc)
    return orig_calloc(nmemb, size);
  else if (_size == 0) {
    errno = 0;
    return NULL;
  }

  void* ret = pms_malloc(_size);
  int savederrno = errno;

  if (ret)
    bzero(ret, _size);

  errno = savederrno;
  return ret;
};

void* realloc(void* ptr, size_t size) {
  DEBUG("called realloc %p, %zu\n", ptr, size);
  pms_init();

  if (!ptr)
    return malloc(size);

  if (size <= 0) {
    free(ptr);
    return NULL;
  }

  char name[MAX_NAME_LENGTH];
  struct stat status;
  size_t oldSize;

  int isOurs = pms_find(ptr, &status, name);

  if (isOurs)
    oldSize = status.st_size;
  else
    oldSize = malloc_usable_size(ptr);

  size_t toCopy = size > oldSize ? oldSize : size;

  int willBeOurs = (size >= pms_min_size);

  if (willBeOurs)
    size = pms_adjust_size(size);

  if (isOurs) {
    if (willBeOurs) {
      char path[BUF_SIZE * 2];
      snprintf (path, BUF_SIZE * 2 - 1, "%s/%s", prefix, name);

      if (oldSize < size) {
        void* newAddr = orig_mremap(ptr, oldSize, size, MREMAP_MAYMOVE);
        if (newAddr == MAP_FAILED)
          die ("realloc can not remap for expand");
        int ret = truncate(path, size);
        if (ret == -1)
          die ("realloc can not truncate file for mmap expand");
        if (newAddr != ptr) {
          char newName [MAX_NAME_LENGTH];
          snprintf(newName, MAX_NAME_LENGTH - 1, "%p", newAddr);
          pms_rename(name, newName);
        }
        errno = 0;
        return newAddr;

      }
      else if (oldSize > size) {

        int ret = truncate(path, size);
        if (ret == -1)
          die ("realloc can not truncate file for mmap shrink");
        void* newAddr = orig_mremap(ptr, oldSize, size, MREMAP_MAYMOVE);
        if (newAddr != ptr) {
          die ("OOPS, address changed on shrink");
        }
        if (newAddr == MAP_FAILED)
          die ("realloc can not remap for mmap shrink");
        return newAddr;

      }
      else
        return ptr;

    } else {

      void* ret = orig_malloc(size);
      if (!ret)
        return NULL;
      else {
        memcpy(ret, ptr, toCopy);
        pms_free(ptr, &status, name);
        return ret;
      }
    }
  } else {
    if (willBeOurs) {

      void* ret = pms_malloc(size);
      if (!ret)
        return NULL;
      memcpy(ret, ptr, toCopy);
      orig_free(ptr);
      return ret;

    } else {

      return orig_realloc(ptr, size);

    }
  }
};

void* reallocarray(void* ptr, size_t nmemb, size_t size) {
  DEBUG("called reallocarray %p to %zu * %zu\n", ptr, nmemb, size);
  pms_init();
  size_t allSize = nmemb * size;
  if (size && allSize / size != nmemb) {
    errno = ENOMEM;
    return NULL;
  }

  return realloc(ptr, allSize);
};

void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset) {
  DEBUG("called mmap addr=%p length=%zu prot=%i flags=%i fd=%i offset=%zu\n", addr, length, prot, flags, fd, (size_t)offset);
  pms_init();
  /*
   * !MAP_ANONYMOUS mmaps are already file-backed, we do not hook them.
   * MAP_GROWSDOWN mmaps can not be easily emulated by file truncations,
   *   and they are rare, so we do not hook them.
   * MMAPs < pms_min_size don't interest us, so we skip them.
   *
   * In all the other cases, we change the flags to be file-backed and use pms_malloc.
   */
  if (!(flags & MAP_ANONYMOUS) || flags & MAP_GROWSDOWN || length < pms_min_size)
    return orig_mmap(addr, length, prot, flags, fd, offset);
  
  int fixed_flags = (flags & ~MAP_ANONYMOUS & ~MAP_PRIVATE & ~MAP_SHARED & ~MAP_POPULATE) | MAP_NONBLOCK | MAP_SHARED_VALIDATE;

  char name[MAX_NAME_LENGTH];
  struct stat status;
  if (addr && (flags & MAP_FIXED) && pms_find(addr, &status, name)) {
    DEBUG("  map at %p already exists, and it is ours, re-map\n", addr);
    if (offset)
      die ("non-zero offset is here not supported");

    void* ret;
    int savederrno;

    // open
    int fd = openat(pms_dirfd, name, O_NOATIME|O_LARGEFILE|O_NOCTTY|O_RDWR);
    if (fd < 0)
      die("can not open tmp file for resizing");

    // grow
    if (length > status.st_size) {
      if (ftruncate(fd, length)) {
        die ("can not expand tmp file");
      }
    }

    // map
    ret = orig_mmap(addr, length, prot, fixed_flags, fd, offset);
    savederrno = errno;

    // shrink
    if (length < status.st_size && savederrno) {
      if (ftruncate(fd, length)) {
        die ("can not shrink tmp file");
      }
    }

    // close
    if (close(fd) < 0)
      die ("mmap can not close tmp file");

    // return
    errno = savederrno;
    return ret;

  } else {
    return pms_mmap(addr, length, prot, fixed_flags);
  }
};

int munmap(void* addr, size_t length) {
  DEBUG("called munmap addr=%p length=%zu\n", addr, length);
  pms_init();

  char path[MAX_NAME_LENGTH];
  struct stat status;
  if (pms_find(addr, &status, path)) {
    pms_free(addr, &status, path);
    return 0;
  } else {
    return orig_munmap(addr, length);
  }
};

void* mremap(void *old_address, size_t old_size, size_t new_size, int flags, ...) {
  DEBUG("called mremap old_address=%p old_size=%zu new_size=%zu flags=%i ...\n",
    old_address, old_size, new_size, flags);
  pms_init();

  char path[BUF_SIZE * 2];
  struct stat status;
  if (pms_find(old_address, &status, path)) {

#ifdef MREMAP_DONTUNMAP
    if (flags & MREMAP_DONTUNMAP)
      die ("MREMAP_DONTUNMAP flag is not supported");
#endif

    if (!(flags & MREMAP_MAYMOVE))
      die ("only mremap with MREMAP_MAYMOVE is supported");

    if (flags & MREMAP_FIXED)
      die ("MREMAP_FIXED is not supported");

    if (old_size != status.st_size)
      die ("old_size != status.st_size");

    return realloc(old_address, new_size);

  } else {

    if (flags & MREMAP_FIXED) {
      void* new_address;
      va_list ap;
      va_start(ap, flags);
      new_address = va_arg(ap, void*);
      va_end(ap);
      return orig_mremap(old_address, old_size, new_size, flags, new_address);
    } else {
      return orig_mremap(old_address, old_size, new_size, flags);
    }
  };

};
